class Input:
    def __init__(self, result, **kw):
        self.data = kw
        self.result = result

    def __repr__(self):
        return 'Input(result=%s, data=%s)' % (str(self.result), str(self.data))
