import pytest
import uuid as uuid_lib
import datetime

from twweb.forms import TaskActionForm

from helpers.utils import Input


@pytest.mark.parametrize('test_input', [
    Input(True, uuid=str(uuid_lib.uuid4())),
    Input(True, uuid=uuid_lib.uuid4()),
    # incorrect uuid - removed the last nibble (half byte)
    Input(False, uuid='bd5d5594-4705-4497-b04f-b0249b5df0c'),
    Input(False, uuid=None),
    Input(False, uuid=''),
    Input(False, uuid='0'),
    Input(False, uuid='abc'),
    Input(False, uuid='<1>'),
])
def test_task_id_validation(app, client, test_input):
    @app.route('/', methods=['POST'])
    def index():
        form = TaskActionForm()
        assert form.validate() is test_input.result
        return ''

    client.post('/', data=dict(test_input.data))
