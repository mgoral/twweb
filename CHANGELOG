twweb (0.6.1)

  * Bugfix: Fixed crashes when accessing tags and projects pages and no tags/projects were defined.
  * Bugfix: Fixed a crash when accessing task details without any defined UDA.

 -- Michał Góral <dev@mgoral.org> Sun, 16 Sep 2018 00:45:03 +0200

twweb (0.6.0)

  * Removed contenteditable experiment.
  * Added annotation button to quick actions on task lists.
  * Replaced detail views modal with simple links to task pages.
  * Description is no longer filled-in when adding auto-filled tasks.
  * Bugfix: Fixed adding errorneous parentheses around custom queries.

 -- Michał Góral <dev@mgoral.org> Thu, 13 Sep 2018 09:00:10 +0200

twweb (0.5.0)

  * Changed service worker's default caching strategy to network first.
  * Added projects and tags views.
  * Added "id" parameter to page variables exposed in templates.
  * Moved "Most important" report to dropdown.
  * Bugfix: "Pending" is correctly highlighted when switching between card and
    list view.

 -- Michał Góral <dev@mgoral.org> Tue, 26 Jun 2018 21:12:26 +0200

twweb (0.4.0)

  * BREAKING: Read user configuration from TWWEB_SETTINGS envvar.
  * BREAKING: Runtime configuration validation.
  * BREAKING: Changed config-dev to default config.
  * Added table view for tasks. It is now a default one.
  * Added "Remember me" to login process.
  * Chosen views are saved in a session.
  * Expire CSRF token with a session by default (previously: every hour).
  * Preserve some form data between requests.
  * Version service worker's cache.
  * Added offline page for PWA.
  * Updated service worker to a different caching strategies, depending on a
    requested resource (usually cache-first, with update from the network in
    background)
  * Added a logo to the navbar.
  * Changed text color for completed tasks.
  * Added back background_color to manifest.json
  * Minor accessability and performance fixes.
  * Bugfix: Close <li> tag.
  * Bugfix: Register service worker only in browsers which support it.

 -- Michał Góral <dev@mgoral.org> Sat, 16 Jun 2018 00:28:09 +0200

twweb (0.3.0)

  * Moved source code to Gitlab.
  * Separate common jinja variables into page and user groups.
  * Render nicer error pages (for 404, failed login or CSRF validation).
  * Improved accessability of the site.
  * Show task projects in a card view.
  * Changed theme-color to match navbar.
  * Added a footer to templates.
  * Added a heading to each page.
  * Bugfix: Disallow caching of some important pages with CSRF tokens.
  * Bugfix: Fixed style.css URI.
  * Bugfix: Fixed incorrect action forms names inside templates.
  * Bugfix: Fixed duplicate input ids.

 -- Michał Góral <dev@mgoral.org> Wed, 06 Jun 2018 23:07:50 +0200

twweb (0.2.2)

  * Bugfix: Added missing icons for PWA

 -- Michał Góral <dev@mgoral.org> Fri, 01 Jun 2018

twweb (0.2.1)

  * Implemented Progressive Web App

 -- Michał Góral <dev@mgoral.org> Fri, 01 Jun 2018

twweb (0.1.2)

  * BREAKING: Renamed everything in example configuration files from tww to
    twweb.
  * Added README.
  * Bugfix: Fixed module name in uwsgi configuration.

 -- Michał Góral <dev@mgoral.org> Thu, 31 May 2018

twweb (0.1.1)

  * Bugfix: Added missing files to pypi package.

 -- Michał Góral <dev@mgoral.org> Thu, 31 May 2018

twweb (0.1.0)

  * Initial release
  * Add and edit tasks
  * Filter and sort tasks

 -- Michał Góral <dev@mgoral.org> Thu, 31 May 2018
